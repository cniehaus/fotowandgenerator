# Purpose of the software

This software takes a list of names (from `people.csv`) and generates pages of PDF-files. 

In the current state it generates a 6x4 page 8 times. The endresult consists of 8 A3-pages of 192 images.

```
1   2   3   4   5   6   | 7   8   9   10  11  12  |13  14  15  16  17  18  |19  20  21  22  23  24
25  26  27  28  29  30  | 31  32  33  34  35  36  |37  38  39  40  41  42  |43  44  45  46  47  48
49  50  51  52  53  54  | 55  56  57  58  59  60  |61  62  63  64  65  66  |67  68  69  70  71  72
73  74  75  76  77  78  | 79  80  81  82  83  84  |85  86  87  88  89  90  |91  92  93  94  95  96
---------------------------------------------------------------------------------------------------
97  98  99  100 101 102 | 103 104 105 106 107 108 |109 110 111 112 113 114 |115 116 117 118 119 120
121 122 123 124 125 126 | 127 128 129 130 131 132 |133 134 135 136 137 138 |139 140 141 142 143 144
145 146 147 148 149 150 | 151 152 153 154 155 156 |157 158 159 160 161 162 |163 164 165 166 167 168
169 170 171 172 173 174 | 175 176 177 178 179 180 |181 182 183 184 185 186 |187 188 189 190 191 192

```

# TODO
- [ ] Translate everything to englisch
- [ ] Demodata
  - [ ] More fake images
  - [ ] Add more information (for example "Janitor" and such)
  - [ ] More diverse name (with fancy character like `ş` to test UTF8)
- [ ] Make it more flexible
  - [ ] Allow other matrices (this is `(6x4) x 4 x 2` but others might prefer far less images)
  - [ ] The black-white should be optional

# Sample Images

All images have been taken from https://this-person-does-not-exist.com/en and are not real photos.

# Running the software

All you need to do is run `python .\picturewallgenerator.py`. The script is known to work with Python 3.6+.

The script generates the black and white images first and then the PDF-files.

```bash
PS \fotowandgenerator> python .\picturewallgenerator.py
Input    images\aco.jpg         Output   images_gray\aco.jpg
Input    images\ada.jpg         Output   images_gray\ada.jpg
Input    images\bry.jpg         Output   images_gray\bry.jpg
Input    images\but.jpg         Output   images_gray\but.jpg
Input    images\car.jpg         Output   images_gray\car.jpg
Input    images\gal.jpg         Output   images_gray\gal.jpg
Input    images\gre.jpg         Output   images_gray\gre.jpg
Input    images\heb.jpg         Output   images_gray\heb.jpg
Input    images\lit.jpg         Output   images_gray\lit.jpg
Input    images\mur.jpg         Output   images_gray\mur.jpg
Input    images\och.jpg         Output   images_gray\och.jpg
Input    images\placeholder.jpg         Output   images_gray\placeholder.jpg
Input    images\sal.jpg         Output   images_gray\sal.jpg
Input    images\sos.jpg         Output   images_gray\sos.jpg
Input    images\tat.jpg         Output   images_gray\tat.jpg
Input    images\wes.jpg         Output   images_gray\wes.jpg
Input    images\wil.jpg         Output   images_gray\wil.jpg
PS \fotowandgenerator> 
```